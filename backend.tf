terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.53.0"
    }
  }

  backend "azurerm" {
    resource_group_name  = "your_reosurce_group"
    storage_account_name = "your_storage_account_name"
    container_name       = "tfstate" //you can change the name if needed
    key                  = "terraform.tfstate" //you can change the name if needed
    subscription_id      = "your_subscription_id"
    tenant_id            = "your_tenant_id"
  }
}

provider "azurerm" {

  features {}

  client_id                  = var.AZURE_AD_CLIENT_ID
  client_secret              = var.AZURE_AD_CLIENT_SECRET
  tenant_id                  = var.AZURE_AD_TENANT_ID
  subscription_id            = var.AZURE_SUBSCRIPTION_ID
  skip_provider_registration = true
}

data "azurerm_client_config" "current" {}