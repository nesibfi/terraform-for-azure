// Storage account NAME
variable "INFRA_ENVIRONMENT" {}

// Resource group name
variable "RESOURCE_GROUP_NAME" {}

// Azure AD client id
variable "AZURE_AD_CLIENT_ID" {}

// Azure subscription id
variable "AZURE_SUBSCRIPTION_ID" {}

// Azure AD tenant id
variable "AZURE_AD_TENANT_ID" {}

// Resource prefix to be used
variable "RESOURCE_PREFIX" {}

// resource location
variable "RESOURCE_LOCATION" {}


//------------------resource specific ------------------//

// app service plan os type
variable "appserviceplan_os_type" {}

// app service plan sku name
variable "appserviceplan_sku_name" {}


//------------------SECRETS------------------//
// Azure AD client secret
variable "AZURE_AD_CLIENT_SECRET" {
  sensitive = true
}
