# Create an app service plan
resource "azurerm_service_plan" "appserviceplan" {
  name                = "${var.RESOURCE_PREFIX}-appservplan-${var.INFRA_ENVIRONMENT}"
  location            = var.RESOURCE_LOCATION
  resource_group_name = var.RESOURCE_GROUP_NAME
  os_type             = var.appserviceplan_os_type
  sku_name            = var.appserviceplan_sku_name
  worker_count        = 1
}
