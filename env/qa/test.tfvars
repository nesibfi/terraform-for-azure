INFRA_ENVIRONMENT     = "qa" //change based on your environement
RESOURCE_GROUP_NAME   = "your_reource_group_name"
AZURE_AD_CLIENT_ID    = "your_client_id"
AZURE_SUBSCRIPTION_ID = "your_subscription_id"
AZURE_AD_TENANT_ID    = "your_tenant_id"
RESOURCE_PREFIX       = "your_prefix_key"
RESOURCE_LOCATION     = "West Europe" //change based on your location

appserviceplan_os_type  = "Linux" //change based on your need
appserviceplan_sku_name = "B1" //change based on your need
